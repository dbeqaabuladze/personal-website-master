const sinon = require("sinon");

const { expect } = require("chai");
const {
  validate,
  validateAsync,
  validateWithThrow,
  validateWithLog,
} = require("../email-validator");

describe("first test", () => {
  it("should return 2", () => {
    expect(2).to.equal(2);
  });
});
describe("Email Validator", () => {
  it("should validate correct email", () => {
    const email = "example@gmail.com";
    const result = validate(email);
    expect(result).to.equal("Email is valid!");
  });

  it("should not validate incorrect email format", () => {
    const email = "invalid-email-format";
    const result = validate(email);
    expect(result).to.be.undefined;
  });

  it("should not validate email with invalid domain", () => {
    const email = "example@invalid.com";
    const result = validate(email);
    expect(result).to.be.undefined;
  });
  it("should not validate email with only correct ending", () => {
    const email = "@gmail.com";
    const result = validate(email);
    expect(result).to.be.undefined;
  });
});
describe("Email Validator Async", () => {
  it("should reject invalid email format", async () => {
    try {
      await validateAsync("invalid-email");
    } catch (error) {
      expect(error).to.be.an("error");
      expect(error.message).to.equal("Invalid email format");
    }
  });
  it("should resolve with true for valid email with valid ending", async () => {
    const result = await validateAsync("testuser@gmail.com");
    expect(result).to.be.true;
  });
  it("should resolve with false for valid email with invalid ending", async () => {
    const result = await validateAsync("testuser@invalid.com");
    expect(result).to.be.false;
  });
});

describe("Email Validator with Throw", () => {
  it("should return true for a valid email", () => {
    const validEmail = "example@gmail.com";
    expect(validateWithThrow(validEmail)).to.be.true;
  });

  it("should throw an error for an invalid email", () => {
    const invalidEmail = "invalid-email";
    expect(() => validateWithThrow(invalidEmail)).to.throw(
      "The provided email is invalid."
    );
  });
  it("should throw an error for an email with an invalid ending", () => {
    const emailWithInvalidEnding = "example@invalid.com";
    expect(() => validateWithThrow(emailWithInvalidEnding)).to.throw(
      "The provided email is invalid."
    );
  });
});

describe("Email Validator with Log", () => {
  let consoleLogStub;

  beforeEach(() => {
    consoleLogStub = sinon.stub(console, "log");
  });

  afterEach(() => {
    consoleLogStub.restore();
  });

  it("should log 'Email is valid!' for valid email", () => {
    validateWithLog("testuser@gmail.com");
    expect(consoleLogStub.calledWith("Email is valid!")).to.be.true;
  });

  it("should return 'Email is valid!' for valid email", () => {
    const result = validateWithLog("testuser@gmail.com");
    expect(result).to.equal("Email is valid!");
  });
});
