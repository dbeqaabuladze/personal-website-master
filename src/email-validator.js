const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

const EMAIL_REGEX = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;

function validate(email) {
  for (const ends of VALID_EMAIL_ENDINGS)
    if (EMAIL_REGEX.test(email) && email.endsWith(ends)) {
      return "Email is valid!";
    }
}

function validateAsync(email) {
  return new Promise((resolve, reject) => {
    if (!EMAIL_REGEX.test(email)) {
      reject(new Error("Invalid email format"));
    } else {
      const isValid = VALID_EMAIL_ENDINGS.some((end) => email.endsWith(end));
      resolve(isValid);
    }
  });
}

function validateWithThrow(email) {
  if (
    VALID_EMAIL_ENDINGS.some(
      (end) => EMAIL_REGEX.test(email) && email.endsWith(end)
    )
  ) {
    return true;
  } else {
    throw new Error("The provided email is invalid.");
  }
}

function validateWithLog(email) {
  const result = validate(email);
  console.log(result);
  return result;
}

module.exports = {
  validate,
  validateAsync,
  validateWithThrow,
  validateWithLog,
};
