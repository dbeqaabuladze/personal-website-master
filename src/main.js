import SectionCreator from "../join-us-section.js";
import {
  validate,
  validateAsync,
  validateWithThrow,
  validateWithLog,
} from "./email-validator.js";
import "./styles/style.css";
import "./WebsiteSection.js";

document.addEventListener("DOMContentLoaded", () => {
  const sectionCreator = SectionCreator;
  const afterDiv = document.querySelector(".app-section--image-culture");
  const standardSection = sectionCreator.create("standard");
  afterDiv.insertAdjacentElement("afterend", standardSection);
  const form = document.querySelector(".joinProgramForm");
  const subscribeButton = form.querySelector(".join-button");
  const emailInput = document.querySelector(".join-input");

  const handleFormSubmission = (event) => {
    event.preventDefault();
    const userInput = emailInput.value;
    // validateWithLog*
    // if (validateWithLog(userInput)) {
    //   emailInput.style.display = "none";
    //   subscribeButton.textContent = "Unsubscribe";
    //   subscribeButton.style.margin = "0 auto";
    //   localStorage.setItem("isSubscribed", "true");
    //   const result = validate(userInput);
    //   console.log(result);
    //   return result;
    // }
    // validateWithThtow*
    // try {
    //   validateWithThrow(userInput);
    //   emailInput.style.display = "none";
    //   subscribeButton.textContent = "Unsubscribe";
    //   subscribeButton.style.margin = "0 auto";
    //   localStorage.setItem("isSubscribed", "true");
    // } catch (error) {
    //   console.error(error);
    // }
    // just validate*
    // if (validate(userInput) === "Email is valid!") {
    //   emailInput.style.display = "none";
    //   subscribeButton.textContent = "Unsubscribe";
    //   subscribeButton.style.margin = "0 auto";
    //   localStorage.setItem("isSubscribed", "true");
    // }

    // validateAsunc*
    // validateAsync(userInput)
    //   .then((isValid) => {
    //     if (isValid) {
    //       emailInput.style.display = "none";
    //       subscribeButton.textContent = "Unsubscribe";
    //       subscribeButton.style.margin = "0 auto";
    //       localStorage.setItem("isSubscribed", "true");
    //     }
    //   })
    //   .catch((error) => {
    //     console.error("an error occussred", error);
    //   });
  };

  form.addEventListener("submit", handleFormSubmission);

  emailInput.addEventListener("input", () => {
    localStorage.setItem("subscriptionEmail", emailInput.value);
  });

  const savedEmail = localStorage.getItem("subscriptionEmail");
  if (savedEmail) {
    emailInput.value = savedEmail;
  }
  const isSubscribed = localStorage.getItem("isSubscribed");
  if (isSubscribed === "true") {
    emailInput.style.display = "none";
    subscribeButton.textContent = "Unsubscribe";
    subscribeButton.style.margin = "0 auto";
  }

  subscribeButton.addEventListener("click", () => {
    if (subscribeButton.textContent === "Unsubscribe") {
      emailInput.style.display = "block";
      emailInput.value = "";
      subscribeButton.textContent = "Subscribe";
      subscribeButton.style.margin = "0";
      localStorage.removeItem("isSubscribed");
      localStorage.removeItem("subscriptionEmail");
    }
  });
});
