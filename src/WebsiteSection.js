class WebsiteSection extends HTMLElement {
  constructor() {
    super();

    const title = this.getAttribute("title");
    const description = this.getAttribute("description");
    const shadow = this.attachShadow({ mode: "open" });
    const section = document.createElement("section");
    section.className = "app-section";
    section.innerHTML = `
      <h2 class="app-title">${title}</h2>
      <h3 class="app-subtitle">${description}</h3>
      <slot></slot>
    `;

    shadow.appendChild(section);
  }
}

customElements.define("website-section", WebsiteSection);
