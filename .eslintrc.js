module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: "standard",
  overrides: [
    {
      env: {
        node: true,
      },
      files: [".eslintrc.{js,cjs}"],
      parserOptions: {
        sourceType: "script",
      },
    },
  ],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  rules: {
    semi: "off", // Disable the 'semi' rule (no semicolons) for the entire project
    indent: ["error", 4], // Enforce 4-space indentation and show error if not followed
    quotes: ["error", "single"], // Enforce single quotes for strings and show error if not used
    "no-console": "warn", // Show warning for any 'console.log' statements
    "no-unused-vars": "off", // Disable the 'no-unused-vars' rule (allow unused variables) for the entire project
    "comma-dangle": ["error", "always-multiline"], // Require trailing commas in multiline object/array and show error if not used
    "arrow-parens": ["error", "as-needed"], // Require parentheses around arrow function parameters only when needed
    "no-mixed-operators": "error", // Show error for mixed binary operators (e.g., '+', '-', '*', '/') without parentheses
    "prefer-template": "error", // Show error when using string concatenation instead of template literals
    "class-methods-use-this": "off", // Disable the rule that enforces class methods to use 'this'
  },
};
