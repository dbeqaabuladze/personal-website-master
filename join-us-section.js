const SectionCreator = (function () {
  function createSection(type) {
    const newSection = `
      <section class="app-section app-section--dynamicallyadded">
        <h1 class="join-headline">${
          type === "standard" ? "Join Our Program" : "Join Our Advanced Program"
        }</h1>
        <p class="join-content">Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.</p>
        <form class="joinProgramForm">
          <div> 
            <input class="join-input" type="text" placeholder="email">
            <button type="submit" class="join-button">${
              type === "standard"
                ? "SUBSCRIBE"
                : "Subscribe to Advanced Program"
            }</button>
          </div>
        </form>
      </section>
    `;

    const section = document.createElement("section");
    section.innerHTML = newSection;

    section.remove = () => {
      section.parentNode.removeChild(section);
    };

    return section;
  }

  return {
    create: function (type) {
      if (type !== "standard" && type !== "advanced") {
        throw new Error("Invalid section type.");
      }

      return createSection(type);
    },
  };
})();

export default SectionCreator;
